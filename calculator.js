// WISH CALCULATOR

const readConsole = () => {
  const read = require('readline-sync');
  
  let calculLine = read.question("Calcul :");

  return calculLine
}

const setup = (calculLine) => {
  const OPERATOR_REGEX = /([+/*-])/g
  const NUMBER_REGEX = /([0-9]*[.])?[0-9]+/g
  
  // Gets the operator of the given string
  let operatorMatch = calculLine.match(OPERATOR_REGEX)
  // Gets the numbers of the given string
  let numbers = calculLine.match(NUMBER_REGEX)
  
  if(!operatorMatch || operatorMatch.length !== 1){
    return 'Unable to found operator, valid operators are : +, -, /, *")'
  }
  if(numbers.length !== 2) {
    return 'Only two number are required'
  }

  // Mapper to calcul
  const mapperCalcul = {
    '+': (number1, number2) => { return number1 + number2},
    '-': (number1, number2) => { return number1 - number2},
    '*': (number1, number2) => { return number1 * number2},
    '/': (number1, number2) => { return number1 / number2},
  }
  // Adds '+' before variable to convert it to a number
  return mapperCalcul[operatorMatch[0]](+numbers[0], +numbers[1])
  
}

module.exports = {
  readConsole,
  setup
}