const { setup } = require('./calculator');

// test with +
test('Adding two numbers', async() => {
  expect(await setup('5+5')).toStrictEqual(10)
  expect(await setup('5.2+1.2')).toStrictEqual(6.4)
})

// test with -
test('Subtracting two numbers', async() => {
  expect(await setup('5-5')).toStrictEqual(0)
  expect(await setup('5.2-1.2')).toStrictEqual(4)
})

// test with *
test('Multiplying two numbers', async() => {
  expect(await setup('5*5')).toStrictEqual(25)
  expect(await setup('5.2*1.2')).toStrictEqual(6.24)
})

// test with /
test('Divided two numbers', async() => {
  expect(await setup('5/5')).toStrictEqual(1)
  expect(await setup('10/2')).toStrictEqual(5)
})